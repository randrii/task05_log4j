package com.epam.online.rybka;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    public static final String ACCOUNT_SID = "ACfe3d95a848d547acaaa69bdbf57229d0";
    public static final String AUTH_TOKEN = "5b601af873ed5d76cbc48487d5382562";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380974236234"),
                        new PhoneNumber("+19283465850"), str).create();
    }
}
