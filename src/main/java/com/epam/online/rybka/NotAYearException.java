package com.epam.online.rybka;

public class NotAYearException extends Exception {
    public NotAYearException(String message) {
        super(message);
    }
}
