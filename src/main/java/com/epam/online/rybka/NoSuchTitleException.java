package com.epam.online.rybka;

public class NoSuchTitleException extends RuntimeException {
    public NoSuchTitleException(String message) {
        super(message);
    }
}
